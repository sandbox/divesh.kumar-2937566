<?php

namespace Drupal\cortex_api_layer\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Client;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "cortex_cart_resource",
 *   label = @Translation("Cortex Cart Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/cart",
 *     "https://www.drupal.org/link-relations/create" = "/api/cart/add",
 *   }
 * )
 */
class CortexCartResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  protected $configManager;
  protected $httpClient;
  protected $cortexApi;
  protected $productService;
  protected $cortexApiUrlService;

  /**
   * Constructs a new CortexCartResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
  array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->httpClient = new Client();
    $this->configManager = \Drupal::config('cortex_api_layer.cortexapiconfig')->get();
    $this->cortexApi = \Drupal::service('cortex_api_layer.access_token');
    $this->productService = \Drupal::service('cortex_api_layer.product');
    $this->cortexApiUrlService = \Drupal::service('cortex_api_layer.urls');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
                $configuration, $plugin_id, $plugin_definition, $container->getParameter('serializer.formats'), $container->get('logger.factory')->get('cortex_api_layer'), $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    // // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if(!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }
    // // Access Token SErvice.
    $cart_url = $this->cortexApiUrlService->getCartUrl();
    $zoom = '?zoom=discount,lineitems:element,lineitems:element:item:definition,lineitems:element:price,lineitems:element:total,total, order:tax, order:total';
    // Execute the http request.
    try {
      $request = $this->httpClient->get($cart_url . $zoom, [
        'headers' => [
          'Authorization' => $this->cortexApi->getAuthorizationHeader(),
        ],
                  ]
      );
    }
    catch(\Exception $e) {
      drupal_set_message(t($e->getMessage()), 'error', TRUE);
    }

    $response = $request->getBody()->getContents();
    // kint($response);die;
    $response = json_decode($response, TRUE);
    $cart_id = end(explode("/", $response['self']['href']));
    $current_cart_url = $response['self']['href'];
    $cart_output = [];
    $current_cart_uri = $response['self']['uri'];
    // If at least a single item is added to cart.
    if($response['total-quantity'] > 0) {
      // Items entered in cart.
      $cart_output['items'] = !empty($response['_lineitems']) ? $this->getCartItems($response['_lineitems']) : [];
      // Total quantity of items in cart.
      $cart_output['total-quantity'] = $response['total-quantity'];
      // Total Amount of the cart.
      $cart_output['total'] = !empty($response['_total']) ? $this->getCartTotal($response['_total']) : [];
      // Total discount applied to the cart.
      $cart_output['discount'] = !empty($response['_discount']) ? $this->getCartDiscount($response['_discount']) : [];
      // Getting Cart Order ID to get taxes and other information.
      $cart_output['taxes'] = !empty($response['_order']) ? $this->getOrderTaxes($response['_order']) : '';
      // Getting Order Total inclusive of all taxes.
      $cart_output['order_total'] = !empty($response['_order']) ? $this->getOrderTotal($response['_order']) : '';
    }
    // Converting the output to REST Response compatible.
    $response = new ResourceResponse($cart_output);
    // Disabling the cache response for the user.
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);
    // Add cacheability metadata to the response, so it shows the real details everytime.
    $response->addCacheableDependency($disable_cache);
    return $response;
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post() {
    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if(!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }
    $data = \Drupal::request()->getContent();
    // If !empty Data then transform it into equavalent JSON.
    if(!empty($data)) {
      $data = json_decode($data, TRUE);
      // Error Handling.
      if(empty($data['quantity']) || empty($data['item_code'])) {
        \Drupal::logger('Cortex:API:CartAdd')->error(t('Both Quantity & Item_Code are required.'));
        return new ResourceResponse(['error' => 'Both Quantity & Item_Code are required.']);
      }
      // Calling Add to Cart.
      $cart = $this->addToCart($this->cortexApi->getCartId(), $data['item_code'], $data['quantity']);

      return new ResourceResponse($cart);
    }

    return new ResourceResponse(['error' => 'Cart not updated.']);
  }

  /**
   * Responds to PUT requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function put() {

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if(!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    // Getting the store endpoint.
    $cortex_api_endpoint = $this->configManager['cortex_api_endpoint'];
    // Getting the Configured Store ID.
    $store_id = $this->configManager['cortex_store_id'];
    // Cart URI.
    $cart_url = $this->cortexApiUrlService->getCartUrl();
    // Composing the URL to add new product to the cart.
    $cart_add_url = $cart_url . '/lineitems/items/' . $store_id . '/' . $item . '?followlocation';
    // Handling the request with exception.
    try {
      // Request to add items to the user's current cart.
      $request = $this->httpClient->put($cart_add_url, [
        'headers' => [
          'Authorization' => $this->cortexApi->getAuthorizationHeader(),
          'Content-Type' => 'application/json',
        ],
        'body' => '{"quantity": ' . $quantity . '}',
      ]);
    }
    catch(\Exception $ex) {
      print $ex->getMessage();
      exit;
    }
    // Getting the Cart Response.
    $response = json_decode($request->getBody()->getContents(), TRUE);

    return new ResourceResponse("Implement REST State PUT!");
  }

  /**
   * Responds to DELETE requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function delete() {
    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if(!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    $data = \Drupal::request()->getContent();
    if(!empty($data)) {
      $json_data = json_decode($data);

      // Get the cart item to remove.
      $lineitem_id = isset($json_data->lineitem_id) ? $json_data->lineitem_id : '';
      if(!empty($lineitem_id)) {
        $cart_delete_url = $this->getCurrrentCartUrl() . '/lineitems/' . $lineitem_id;

        // Call the cortex API to remove the cart item.
        try {
          $request = $this->httpClient->delete($cart_delete_url, [
            'headers' => [
              'Authorization' => $this->cortexApi->getAuthorizationHeader(),
            ],
          ]);
        }
        catch(\Exception $ex) {
          $message = $ex->getMessage();
          \Drupal::logger('Cortex:API:CartRemove')->error($message);
          $response = ['status' => 'error', 'message' => $message];
          print_r(json_encode($response));
          exit;
        }

        // Status code 204 indicates the item has been sucessfully removed from cart.
        if($request->getStatusCode() == 204) {
          $response = [
            'code' => $request->getStatusCode(),
            'status' => 'success',
            'message' => 'The item has been successfully removed from your cart!'
          ];
        }
        else {
          $response = [
            'code' => $request->getStatusCode(),
            'status' => 'error',
            'message' => 'Error while removing the item from your cart!'
          ];
        }
      }
    }
    print_r(json_encode($response));
    exit;
  }

  /**
   *
   */
  protected function addToCart($cart_id, $item, $quantity) {
    // Getting the store endpoint.
    $cortex_api_endpoint = $this->configManager['cortex_api_endpoint'];
    // Getting the Configured Store ID.
    $store_id = $this->configManager['cortex_store_id'];
    // Cart URI.
    $cart_url = $this->cortexApiUrlService->getCartUrl();

    // Composing the URL to add new product to the cart.
    $cart_add_url = $cart_url . '/lineitems/items/' . $store_id . '/' . $item;
    // Handling the request with exception.
    try {
      // Request to add items to the user's current cart.
      $request = $this->httpClient->post($cart_add_url, [
        'headers' => [
          'Authorization' => $this->cortexApi->getAuthorizationHeader(),
          'Content-Type' => 'application/json',
        ],
        'body' => '{"quantity": ' . $quantity . '}',
      ]);
    }
    catch(\Exception $ex) {
      \Drupal::logger('Cortex:API:CartAdd')->error($ex->getMessage());
      print $ex->getMessage();
      exit;
    }
    // // Getting the Cart Response.
    //    $response = json_decode($request->getBody()->getContents(), TRUE);
    //
    //    return $this->formatCartResponse($response);
    if(in_array($request->getStatusCode(), [200, 201])) {
      return ['code' => $request->getStatusCode(), 'message' => t('Item successfully added to the cart!')];
    }
    return ['code' => $request->getStatusCode(), 'message' => t('ERROR: Item did not get added to the cart')];
  }

  /**
   * This method returns a cart's Total Amount.
   *
   * @param mixed $current_cart_uri
   *   URI for the current Cart.
   * @param mixed $cart_id
   *   Optional Cart ID.
   *
   * @return mixed
   *   Array containing cart total and amount.
   */
  protected function getCartTotal($total) {
    // If total object is not empty then process ahead and return accordingly.
    if(!empty($total)) {
      $total = array_shift($total);
      // Return the discount.
      return array_shift($total['cost']);
    }

    return [];
  }

  /**
   * This method returns a cart Discount.
   *
   * @param mixed $current_cart_uri
   *   URI for the current Cart.
   * @param mixed $cart_id
   *   Optional Cart ID.
   *
   * @return mixed
   *   Array containing cart total and amount.
   */
  protected function getCartDiscount($discount) {
    // Check if discount is available and return accordingly.
    if(!empty($discount)) {
      $discount = array_shift($discount);
      // Return the discount.
      return array_shift($discount['discount']);
    }

    return [];
  }

  /**
   * This method returns the cart items added to the cart.
   *
   * @param mixed $cart_id
   */
  protected function getCartItems($lineitems) {
    $cart_items = [];
    foreach($lineitems as $lineitem) {
      // Traversing each item.
      foreach($lineitem as $element) {
        // Traversing each definition.
        foreach($element as $element_item) {
          // Lineitem ID to use for cart removal or update.
          $lineitem_id = end(explode('/', $element_item['self']['uri']));
          // Get the first item node.
          $item = !empty($element_item['_item']) ? array_shift($element_item['_item']) : [];
          // Return back item has no information.
          if(empty($item)) {
            return [];
          }

          // Item Definition.
          $definition = !empty($item['_definition']) ? array_shift($item['_definition']) : [];

          // Item Code.
          $item_code = end(explode('/', $definition['self']['uri']));
          $sku_info = $this->getSkuTitle($definition);
          // Product Title.
          $product_title = !empty($sku_info['label']) ? $sku_info['label'] : $definition['display-name'];
          $item_info['options']['Size'][] = $sku_info['size'];
          $item_info['options']['Print'][] = $product_title;
          $product_title = !empty($sku_info['size']) ? $product_title . " (" . $sku_info['size'] . ")" : $product_title;
          // Item Price processing.
          $price_information = !empty($element_item['_price']) ? array_shift($element_item['_price']) : [];
          // List price of item.
          $item_price['list-price'] = !empty($price_information['list-price']) ? array_shift($price_information['list-price']) : [];
          // Purchase price of item on which the item is bought.
          $item_price['purchase-price'] = !empty($price_information['purchase-price']) ? array_shift($price_information['purchase-price']) : [];
          // Cart total for this individual Item.
          $item_cart_total = !empty($element_item['_total']) ? array_shift($element_item['_total']) : [];
          $item_total_cost = !empty($item_cart_total['cost']) ? array_shift($item_cart_total['cost']) : [];

          // Composing the final array.
          $item_info['itemcode'] = $item_code;
          $item_info['lineitem_id'] = $lineitem_id;
          $item_info['product-name'] = $product_title;
          $item_info['prices'] = $item_price;
          $item_info['quantity'] = $element_item['quantity'];
          $item_info['cart_total'] = $item_total_cost;

          $cart_items[] = $item_info;
        }
      }
    }
    return $cart_items;
  }

  /**
   *
   */
  protected function getSkuTitle($details) {
    // Fetching the label for SKU.
    if(!empty($details['self']['uri'])) {
      $item_code = array_pop(explode("/", $details['self']['uri']));
      // Loading node by a property.
      $sku_information = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        'field_itemcode' => $item_code,
        'type' => 'product_sku',
      ]);
      $sku_information = array_shift($sku_information);
      $term = $sku_information->field_product_sizes->getString();

      $output = [
        'label' => $sku_information->getTitle(),
      ];
      // Fetching the term.
      if(!empty($term)) {
        $term_data = \Drupal\taxonomy\Entity\Term::load($term);
        $output['size'] = !empty($term_data) ? ucwords($term_data->getName()) : '';
      }

      // Fetching the very first information assuming there is no duplicate data.
      // TODO: this code needs to be make more intellingent .
      return $output;
    }
    return '';
  }

  /**
   * To get the current cart url
   */
  private function getCurrrentCartUrl() {
    $cart_url = $this->cortexApiUrlService->getCartUrl();
    // Execute the http request.
    try {
      $request = $this->httpClient->get($cart_url, [
        'headers' => [
          'Authorization' => $this->cortexApi->getAuthorizationHeader(),
        ],
      ]);
      $response = $request->getBody()->getContents();
      $response = json_decode($response, TRUE);
      $current_cart_url = $response['self']['href'];
      return $current_cart_url;
    }
    catch(\Exception $e) {
      \Drupal::logger('cortex_api_layer')->error($e->getMessage());
    }
  }

  /**
   * This method gives the default order id based on cart info.
   * 
   * @param type $cart_data
   * @return type
   */
  public function getOrderTaxes($order) {
    
    $order_taxes = [];
    // Checking for the order id
    if(!empty(array_column($order, '_tax'))) {
      // Getting the main taxes element.
      $order = array_shift($order);
      // Fetching the order tax.
      $taxes = $order['_tax'];
      // Fetching the order related tax information about tax type and cost.
      $order_taxes = !empty($taxes[0]['cost'][0]) ? $taxes[0]['cost'][0] : [];
    }
    return $order_taxes ;
  }
  
  /**
   * This method returns the Order total inclusive of all cost and taxes.
   * @param type $order
   * @return type
   */
  public function getOrderTotal($order) {
    
    $order_total = [];
    // Checking for the order id
    if(!empty(array_column($order, '_total'))) {
      // Getting the main total element.
      $order = array_shift($order);
      // Fetching the order tax.
      $total = $order['_total'];
      // Fetching the order related tax information about tax type and cost.
      $order_total = !empty($total[0]['cost'][0]) ? $total[0] ['cost'][0] : [];
    }
    return $order_total ;
  }
}
