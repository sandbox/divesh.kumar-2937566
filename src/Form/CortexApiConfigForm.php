<?php

namespace Drupal\cortex_api_layer\Form;

use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

const CORTEX_API_ENDPOINT = 'http://34.215.201.233:9080/cortex';
const STORE_ID = 'geometrixx';
const CART_URI = '/carts';
const PRODUCT_URI = '/items';
const SEARCH_URI = '/search';
const NAVIGATION_URI = '/navigation';
const AUTH_URI = '/oauth2/tokens';
const LOOKUP_URI = '/lookups';
const ACCOUNT_REGISTRATION = '/registration/!store/newaccount/form?followlocation';

/**
 * Class CortexApiConfigForm.
 */
class CortexApiConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cortex_api_layer.cortexapiconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cortex_api_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cortex_api_layer.cortexapiconfig')->get();
    // Gettting list of Vocabularies.
    $vocabs = Vocabulary::loadMultiple();

    $options = [];
    if (!empty($vocabs)) {
      // Fetching the options.
      foreach ($vocabs as $vocab) {
        $options[$vocab->id()] = $vocab->get('name');
      }
    }
    $form['category'] = [
      '#type' => 'fieldset',
      '#description' => $this->t('Select Default Category for Product Mapping at time of product import.'),
    ];

    $form['category']['cortex_product_default_category'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Vocabularies'),
      '#options' => $options,
      '#default_value' => !empty($config['cortex_product_default_category']) ? $config['cortex_product_default_category'] : 'category',
    ];

    $form['cortex_api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex API Endpoint'),
      '#description' => $this->t('Please provide cortex API Endpoint to connect with ElasticPath server.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => !empty($config['cortex_api_endpoint']) ? $config['cortex_api_endpoint'] : CORTEX_API_ENDPOINT,
    ];

    $form['cortex_store_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex API Store ID'),
      '#description' => $this->t('Store ID @ ElasticPath server.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => !empty($config['cortex_store_id']) ? $config['cortex_store_id'] : STORE_ID,
    ];

    $form['cortex_oauth2_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex OAuth2 Endpoint'),
      '#description' => $this->t('Cortex API Authorization Endpoint URI. [Prefix "/"].'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => !empty($config['cortex_oauth2_uri']) ? $config['cortex_oauth2_uri'] : AUTH_URI,
    ];

    $form['cortex_cart_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex API : Cart Endpoint'),
      '#description' => $this->t('Cortex API to manage Cart. [Prefix "/"]'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => !empty($config['cortex_cart_uri']) ? $config['cortex_cart_uri'] : CART_URI,
    ];

    $form['cortex_product_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex API : Product Endpoint'),
      '#description' => $this->t('Cortex API endpoint to get Products. [Prefix "/"]'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => !empty($config['cortex_product_uri']) ? $config['cortex_product_uri'] : PRODUCT_URI,
    ];

    $form['cortex_navigation_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex API : Navigation Endpoint'),
      '#description' => $this->t('Cortex API endpoint to get collections/categories. [Prefix "/"]'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => !empty($config['cortex_navigation_uri']) ? $config['cortex_navigation_uri'] : NAVIGATION_URI,
    ];

    $form['cortex_search_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex API : Search Endpoint'),
      '#description' => $this->t('Cortex API endpoint to get collections/categories. [Prefix "/"]'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => !empty($config['cortex_search_uri']) ? $config['cortex_search_uri'] : SEARCH_URI,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cortex_api_layer.cortexapiconfig')
      ->set('cortex_api_endpoint', $form_state->getValue('cortex_api_endpoint'))
      ->set('cortex_store_id', $form_state->getValue('cortex_store_id'))
      ->set('cortex_cart_uri', $form_state->getValue('cortex_cart_uri'))
      ->set('cortex_product_uri', $form_state->getValue('cortex_product_uri'))
      ->set('cortex_navigation_uri', $form_state->getValue('cortex_navigation_uri'))
      ->set('cortex_search_uri', $form_state->getValue('cortex_search_uri'))
      ->set('cortex_oauth2_uri', $form_state->getValue('cortex_oauth2_uri'))
      ->set('cortex_lookups_uri', $form_state->getValue('cortex_lookups_uri'))
      ->set('cortex_product_default_category', $form_state->getValue('cortex_product_default_category'))
      ->save();
  }

}
