<?php

  namespace Drupal\cortex_api_layer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigManager;

/**
 * Class CortexApiProductImporterForm.
 */
class CortexApiProductImporterForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * Constructs a new CortexApiProductImporterForm object.
   */
  public function __construct(
        EntityTypeManager $entity_type_manager,
  Client $http_client,
  ConfigManager $config_manager
    ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->configManager = $config_manager;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
     $container->get('entity_type.manager'), $container->get('http_client'), $container->get('config.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cortex_api_product_importer_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import Products'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Product Importer Service.
    $product_importer = \Drupal::service('cortex_api_layer.product_importer');
    $product_importer->executeBatchImport('sample');
  }

}
