<?php

  namespace Drupal\cortex_api_layer\Form;

use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use GuzzleHttp\Client;
use Drupal\user\PrivateTempStoreFactory;

/**
 * Class CortexApiProductCategoryImporterForm.
 */
class CortexApiProductCategoryImporterForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Drupal\user\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $userPrivateTempstore;

  /**
   * Constructs a new CortexApiProductCategoryImporterForm object.
   */
  public function __construct(
        ConfigManager $config_manager,
  Client $http_client,
  PrivateTempStoreFactory $user_private_tempstore
    ) {
    $this->configManager = $config_manager;
    $this->httpClient = $http_client;
    $this->userPrivateTempstore = $user_private_tempstore;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.manager'), $container->get('http_client'), $container->get('user.private_tempstore')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cortex_api_product_category_importer_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Gettting list of Vocabularies.
    $vocabs = Vocabulary::loadMultiple();
    $options = [];
    // If No vocabs available then show the appropriate message.
    if (!empty($vocabs)) {
      // Fetching the options.
      foreach ($vocabs as $vocab) {
        $options[$vocab->id()] = $vocab->get('name');
      }
      $form['container'] = [
        '#title' => t('Category Importer Tool'),
        '#type' => 'fieldset',
        '#description' => t('Select a vocabulary from the given list below and import the data from Elastic Path Server.'),
      ];
      // Field to select vocabulary.
      $form['container']['vid'] = [
        '#type' => 'select',
        '#title' => t('Select Vocabulary'),
        '#description' => t('Select an existing vocabulary to import data into.'),
        '#options' => $options,
        '#required' => TRUE,
      ];
      // Import categories.
      $form['import_categories'] = [
        '#type' => 'submit',
        '#value' => $this->t('Import Categories'),
      ];
    }
    else {
      $form['markup'] = [
        '#markup' => t('No existing vocabularies available. Please create one and then initiate import.'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $importer = \Drupal::service('cortex_api_layer.category_importer');
    // URL to get categories.
    try {
      $batch = [
        'title' => t('Importing Product Categories...'),
        'operations' => [
          $importer->executeImport($form_state->getValue('vid')),
        ],
        'finished' => drupal_set_message(t('Category Import Complete.')),
      ];
      batch_set($batch);
    }
    catch (\Exception $e) {
      drupal_set_message(t($e->getMessage()), 'error', TRUE);
    }
  }

}
