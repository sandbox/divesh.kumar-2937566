<?php

  namespace Drupal\cortex_api_layer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;

/**
 * Class CortexApiTokenGeneratorForm.
 */
class CortexApiTokenGeneratorForm extends FormBase {

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a new CortexApiTokenGeneratorForm object.
   */
  public function __construct(
        Client $http_client
    ) {
    $this->httpClient = $http_client;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cortex_api_token_generator_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['generate_access_token'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate Access Token'),
      '#description' => $this->t('Generate Access Token'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $cortex_api_service = \Drupal::service('cortex_api_layer.access_token');
    $batch = [
      'title' => t('Generating Access Token...'),
      'operations' =>
        [
          $cortex_api_service->generate(),
        ],
      'finished' => drupal_set_message("Access Token Generated Successfully!"),
    ];

    batch_set($batch);
  }

}
