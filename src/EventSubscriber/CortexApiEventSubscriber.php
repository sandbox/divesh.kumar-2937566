<?php

namespace Drupal\cortex_api_layer\EventSubscriber;

use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\user\PrivateTempStoreFactory;
use Drupal\Core\Session\AccountProxy;

/**
 * Class CortexApiEventSubscriber.
 */
class CortexApiEventSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\user\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $userPrivateTempstore;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $configFactory;
  protected $cortexApi;

  /**
   * Constructs a new CortexApiEventSubscriber object.
   */
  public function __construct(PrivateTempStoreFactory $user_private_tempstore, AccountProxy $current_user, ConfigFactory $configFactory) {
    $this->userPrivateTempstore = $user_private_tempstore;
    $this->currentUser = $current_user;
    $this->configFactory = $configFactory;
    $this->cortexApi = \Drupal::service('cortex_api_layer.access_token');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['kernel.request'] = ['cortex_api_authenticate'];

    return $events;
  }

  /**
   * This method is called whenever the kernel.request event is
   * dispatched.
   *
   * @param GetResponseEvent $event
   */
  public function cortex_api_authenticate(Event $event) {
    $current_path = \Drupal::service('path.current')->getPath();
    if(!strpos($current_path, 'contenthub')) {
      error_reporting(0);
      // Private tempstorage to to capture session data.
      $private_tempstore = \Drupal::service('user.private_tempstore');
      // Getting Cortex API token.
      $cortex_api_token = $private_tempstore->get('cortex_api_layer')->get('authentication_token');
      // If authentication token is empty then generate the API token.
      if(empty($cortex_api_token)) {
        // Get URI.
        $config = \Drupal::config('cortex_api_layer.cortexapiconfig')->get();
        // Fetching Cortex API key from configurations.
        $url = $config['cortex_api_endpoint'] . $config['cortex_oauth2_uri'];
        // Giving a background call to get POST.
        try {
          $http_client = \Drupal::httpClient()->post($url, [
            'body' => 'grant_type=password&scope=' . $config['cortex_store_id'] . '&role=PUBLIC',
            'headers' => [
              'Content-Type' => 'application/x-www-form-urlencoded',
            ],
          ]);
        }
        catch(\Exception $e) {
          drupal_set_message($e->getMessage(), 'error', TRUE);
        }
        $response = $http_client->getBody()->getContents();
        // If response is not empty then set the authentication token once again.
        if(!empty($response)) {
          $response_decoded = \GuzzleHttp\json_decode($response, TRUE);
          $private_tempstore->get('cortex_api_layer')->set('authentication_token', $response_decoded['access_token']);
          // $this->cortexApi->getCartId();
          \Drupal::logger('Cortex:API:AccessToken')->info(t("Access Token : @token generated for current user.", ['@token' => $response_decoded['access_token']]));
        }
      }
    }
  }

  /**
   *
   */
  public function getAccessToken() {
    // Private tempstorage to to capture session data.
    $private_tempstore = \Drupal::service('user.private_tempstore');
    // Getting Cortex API token.
    $cortex_api_token = $private_tempstore->get('cortex_api_layer');

    return !empty($cortex_api_token) ? $cortex_api_token->get('authentication_token') : '';
  }

}
