<?php

namespace Drupal\cortex_api_layer\services;

use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigManager;

/**
 * Class CortexApiProductService.
 */
class CortexApiProductService {

  /**
   * Drupal\cortex_api_layer\services\CortexApiAccessTokenService definition.
   *
   * @var \Drupal\cortex_api_layer\services\CortexApiAccessTokenService
   */
  protected $cortexApiLayerAccessToken;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  protected $cortexConfig;
  protected $entityManager;

  /**
   * Constructs a new CortexApiProductService object.
   */
  public function __construct(CortexApiAccessTokenService $cortex_api_layer_access_token, Client $http_client, ConfigManager $config_manager) {
    $this->cortexApiLayerAccessToken = $cortex_api_layer_access_token;
    $this->httpClient = $http_client;
    $this->configManager = $config_manager;
    $this->cortexConfig = $this->configManager->getConfigFactory()->getEditable('cortex_api_layer.cortexapiconfig')->get();
    $this->entityManager = \Drupal::service('entity_type.manager');
  }

  /**
   * This method returns a price Micro Service call to fetch prices of an item.
   *
   * @param mixed $item_id
   *   Item ID of a product.
   *
   * @return array
   */
  public function getItemPrice($item_id) {
    // Getting the PRice URL.
    $url = $this->cortexApiLayerAccessToken->getItemPriceUrl($item_id);
    // Exception handling.
    try {
      // Creating a HTTP GET request.
      $request = $this->httpClient->get($url, [
        'headers' => [
          'Authorization' => $this->cortexApiLayerAccessToken->getAuthorizationHeader(),
        ],
      ]);
    }
    catch(\Exception $ex) {
      \Drupal::logger('Cortex:API:Price')->error($ex->getMessage());
      return [];
    }
    // Execute the Response fetch from HTTP Request.
    $response = $request->getBody()->getContents();
    // If not empty response received then return the price informatoion.
    if(!empty($response)) {
      $response = json_decode($response, TRUE);
      return [
        'list-price' => $response['list-price'],
        'purchase-price' => $response['purchase-price'],
      ];
    }
    // Return Empty prices.
    return [];
  }

  /**
   * This method returns a price Micro Service call to fetch prices of an item.
   *
   * @param mixed $item_id
   *   Item ID of a product.
   *
   * @return array
   */
  public function getItemAvailability($item_id) {
    // Getting the Availability URL.
    $url = $this->cortexApiLayerAccessToken->getItemAvailabilityUrl($item_id);
    // Exception Handling the Item Availability.
    try {
      // Creating a HTTP GET request.
      $request = $this->httpClient->get($url, [
        'headers' => [
          'Authorization' => $this->cortexApiLayerAccessToken->getAuthorizationHeader(),
        ],
      ]);
    }
    catch(\Exception $ex) {
      \Drupal::logger('Cortex:ITEM:Availability')->error(t($ex->getMessage()));
      return '';
    }
    // Execute the Response fetch from HTTP Request.
    $response = $request->getBody()->getContents();
    // If not empty response received then return the price informatoion.
    if(!empty($response)) {
      $response = json_decode($response, TRUE);
      return $response['state'];
    }
    // Return Empty prices.
    return '';
  }

  /**
   * This method returns a price Micro Service call to fetch prices of an item.
   *
   * @param mixed $item_id
   *   Item ID of a product.
   *
   * @return array
   */
  public function getItemDefinition($item_id) {
    // Getting the PRice URL.
    $url = $this->cortexApiLayerAccessToken->getItemDefinitionUrl($item_id);
    try {
      // Creating a HTTP GET request.
      $request = $this->httpClient->get($url, [
        'headers' => [
          'Authorization' => $this->cortexApiLayerAccessToken->getAuthorizationHeader(),
        ],
      ]);
    }
    catch(\Exception $ex) {
      \Drupal::logger('Cortex:API:ItemDefinition')->error(t($ex->getMessage()));
      return [];
    }
    // Execute the Response fetch from HTTP Request.
    $response = $request->getBody()->getContents();
    // If not empty response received then return the price informatoion.
    if(!empty($response)) {
      $response = json_decode($response, TRUE);
      // This variable checks if this definition ahs options.
      $has_options = FALSE;
      // This checks for the options for current item and returns a boolean flag.
      foreach($response['links'] as $link) {
        if($link['rel'] == 'options') {
          $has_options = TRUE;
          break;
        }
      }
      return ['display-name' => $response['display-name'], 'has-options' => $has_options];
    }
    // Return Empty prices.
    return '';
  }

  /**
   * This method returns a price Micro Service call to fetch prices of an item.
   *
   * @param mixed $item_id
   *   Item ID of a product.
   *
   * @return array
   */
  public function getItemOptions($item_id) {
    // Getting the PRice URL.
    $url = $this->cortexApiLayerAccessToken->getItemOptionsUrl($item_id);
    // Execption Handling.
    try {
      // Creating a HTTP GET request.
      $request = $this->httpClient->get($url, [
        'headers' => [
          'Authorization' => $this->cortexApiLayerAccessToken->getAuthorizationHeader(),
        ],
      ]);
    }
    catch(\Exception $ex) {
      \Drupal::logger('Cortex:API:Options')->error($ex->getMessage(), 'error', TRUE);
      return [];
    }
    // Execute the Response fetch from HTTP Request.
    $response = $request->getBody()->getContents();
    // Initializing API response array.
    $options = [];
    // If not empty response received then return the price informatoion.
    if(!empty($response)) {
      // Converting response to JSON for easier parsing.
      $response = json_decode($response, TRUE);
      // If Response for request has further links to get information then traverse.
      if(!empty($response['links'])) {
        // If options are available which is matched by [REL=>ELEMENT].
        foreach($response['links'] as $element) {
          // Bypass all other links than element.
          if($element['rel'] != 'element') {
            continue;
          }
          // GEt the options HREF.
          $option_href = $element['href'];
          try {
            // Giving a call to capture the element;.
            $element_request = $this->httpClient->get($option_href, [
              'headers' => [
                'Authorization' => $this->cortexApiLayerAccessToken->getAuthorizationHeader(),
              ],
            ]);
          }
          catch(\Exception $ex) {
            \Drupal::logger('Cortex::API::SingleOption')->error($ex->getMessage());
            return [];
          }
          // Capturing the response from an options' call.
          $element_call_response = $element_request->getBody()->getContents();
          // Convertig JSON to Array.
          $element_call_response = json_decode($element_call_response, TRUE);
          // Traversing Each Element Separately.
          foreach($element_call_response['links'] as $option) {
            // Bypassing all elements rather than value.
            if($option['rel'] != 'value') {
              continue;
            }
            try {
              // Get Option Micro Service CAll.
              $option_value_request = $this->httpClient->get($option['href'], [
                'headers' => [
                  'Authorization' => $this->cortexApiLayerAccessToken->getAuthorizationHeader(),
                ],
              ]);
            }
            catch(\Exception $ex) {
              \Drupal::logger('Cortex:API:Options')->error($ex->getMessage());
              return [];
            }
            // Get Value of and option .
            $option_value_response = $option_value_request->getBody()->getContents();
            $option_value_response = json_decode($option_value_response, TRUE);
            // Return the VAlue for the option/attribute of an item.
            $options[$element_call_response['display-name']][] = $option_value_response['display-name'];
          }
        }
        return $options;
      }
    }
    // Return Empty prices.
    return [];
  }

  /**
   * This method returns multiple items .
   *
   * @param mixed $items
   *
   * @return mixed
   */
  public function getItems($items) {
    $result = [];
    // Traversing all items.
    foreach($items as $item) {
      // Getting items one by one.
      $result[] = $this->getItem($item);
    }
    // Return output.
    return $result;
  }

  /**
   * This method returns a JSON for an item in EP.
   *
   * @param mixed $item
   *
   * @return array
   */
  public function getItem($item) {
    // Get PRoduct definition and options value.
    $item_definition = $this->getItemDefinition($item);
    // This checks if the item is defined.
    if(!empty($item_definition['display-name'])) {
      // Final output array will contain differnt sections to an item.
      $output = [
        'item' => [
          'product-name' => $item_definition['display-name'],
          'availability' => $this->getItemAvailability($item),
          'prices' => $this->getItemPrice($item),
          // Process options only if this item as options.
          'options' => ($item_definition['has-options'] === TRUE) ? $this->getItemOptions($item) : [],
        ],
      ];
      // Return the output of combined items.
      return $output;
    }
    return [];
  }

  /**
   * This method parses the item definition and returns that into a logical associated array.
   *
   * @param array $item
   *   Product Array captured from Elastic Path.
   *
   * @return array
   */
  public function parseItemDefinition($item) {
    $item_definition = [];
    $product_code = "";
    // Parse definition array if it's not empty.
    if(!empty($item['_definition'])) {
      // Traversing the Item Definition to return a logical composition of JSON.
      foreach($item['_definition'] as $definition) {
        // Capturing the Product code for Cart Functions.
        if(!empty($definition['self']['uri']) && in_array($definition['self']['type'], ['elasticpath.items.item', 'elasticpath.itemdefinitions.item-definition'])) {
          // Capturing the URI to get ITEMCODE.
          if(strstr("?zoom", $uri)) {
            // Truncating the URI with Query strings and capturing the itemcode.
            $product_code = end(explode('/', substr($definition['self']['uri'], 0, strpos($uri, "?zoom"))));
          }
          else {
            // Truncating the URI with Query strings and capturing the itemcode.
            $product_code = end(explode('/', $definition['self']['uri']));
          }
        }
        $item_definition['item_code'] = $product_code;
        // Get Item Details.
        $item_definition['display-name'] = $definition['display-name'];
        $item_definition['details'] = $this->parseItemDetails($definition);
        $item_definition['options'] = !empty($definition['_options']) ? $this->parseItemOptions($definition['_options']) : [];
      }
      // Getting price of the item.
      $item_definition['prices'] = !(empty($item['_price'])) ? $this->parseItemPrice($item['_price']) : [];
    }
    return $item_definition;
  }

  /**
   * This method parses the details section of an item's definition returns back
   * response in proper heads.
   *
   * @param array $definition
   *   Product Item Definition.
   *
   * @return array
   *   Properly formed array of response with specific heads to form final output
   *   json.
   */
  public function parseItemDetails($definition = []) {
    // Variable to contain final result.
    $result = [];
    // If definition is not empty.
    if(!empty($definition['details'])) {
      // TRaversing complete details sections.
      foreach($definition['details'] as $detail) {
        // Populating data to different keys bases on the display-name of details
        // that can tags, productname, productcode, and skus for a product.
        switch($detail['display-name']) {
          case 'Tag':
            // This returns a products category in Parent:Child category manner.
            $output['tags'] = $this->getDetailValue($detail);
            break;

          case 'prodName':
            // This returns the Product Name.
            $output['name'] = $this->getDetailValue($detail);
            break;

          case 'ProdCode':
            // This returns the product title.
            $output['code'] = $this->getDetailValue($detail);
            break;

          case 'SKUIDS':
            // This returns the product skus list.
            $output['skus'] = $this->getDetailValue($detail);
            break;

          case 'SKUOPTIONS':
            // This returns the product skus list.
            $output['sku_options'] = $this->getSkuSizes($detail);
            break;
        }
      }
      $result = $output;
    }

    return $result;
  }

  /**
   *
   */
  public function parseItemPrice($price_details) {
    $price_details = array_shift($price_details);
    $price['purchase-price'] = !empty($price_details['purchase-price']) ? array_shift($price_details['purchase-price']) : [];
    $price['list-price'] = !empty($price_details['list-price']) ? array_shift($price_details['list-price']) : [];

    return $price;
  }

  /**
   * This method returns the options for a product.
   *
   * @param mixed $options
   *
   * @return mixed
   */
  public function parseItemOptions($options) {
    $options = array_shift($options);
    $output = [];
    if(!empty($options['_element'])) {
      // Traversing all elements.
      foreach($options['_element'] as $element) {
        // Traversing element values for each element.
        foreach($element['_value'] as $element_value) {
          // Fetchign Element Value.
          $output[$element['name']][] = ['name' => $element_value['name'], 'display-name' => $element_value['display-name']];
        }
      }
    }
    return $output;
  }

  /**
   *
   */
  public function parseItemCategories($categories) {
    $terms = [];
    // If tags are not empty then process category mapping.
    if(!empty($categories)) {
      $default_category = !empty($this->cortexConfig['cortex_product_default_category']) ? $this->cortexConfig['cortex_product_default_category'] : 'category';

      // Traversal of each category as there could be multiple tags.
      foreach($categories as $category) {
        // Exploding the Tags into array.
        $tags = explode(':', $category);
        // Assuming that there is only 2 terms i.e., parent & child.
        $parent = $tags[0];
        $child = !empty($tags[1]) ? $tags[1] : '';
        try {
          $parent_term = $this->entityManager->getStorage('taxonomy_term')->loadByProperties([
            'vid' => $default_category,
            'name' => $parent,
          ]);
        }
        catch(\Exception $ex) {
          \Drupal::logger('Cortex:API:ParseItemCategories')->error($ex->getMessage());
          print $ex->getMessage();
          exit;
        }
        // By Deault it will add to main term.
        $term = 0;
        // If parent term is found then go ahead and traverse the hierarchy.
        if(!empty($parent_term)) {
          $parent_term = array_shift($parent_term);
          $parent_id = $parent_term->id();
          $term = $parent_id;
          // Fetching the child location.
          if(!empty($child)) {
            $term = \Drupal::service('cortex_api_layer.product_importer')->getTermByNameAndParent($child, $parent_id);
          }
        }
        $terms[] = $term;
      }
    }
    return $terms;
  }

  /**
   * This method returns the value for a specific details item in a product's details section.
   *
   * @param array $details
   *   Details object of an item/product.
   *
   * @return array
   *   Value array.
   */
  public function getDetailValue($details) {
    // Checking if value is not empty then return back to parser.
    if(!empty($details['value'])) {
      return $details['value'];
    }
    // Empty Return.
    return [];
  }

  /**
   * This method returs the sku details based on sku id.
   *
   * @param mixed $skus
   */
  public function getSkuSizes($details) {
    $sizes = [];
    $temp_array = [];

    // If sizes are not empty then get the sizes all together.
    if(!empty($details['display-value'])) {
      $values = json_decode($this->fixJson($details['display-value'], TRUE), TRUE);
      // Traverse each size values for multiple information for every sku.
      foreach($values as $key => $value) {
        $sizes[$key] = ['print' => $value['print'], 'sizes' => explode(',', $value['size'])];
      }
    }
    // Actual elements that would be inserted.
    return $sizes;
  }

  /**
   * This method for now fixing the JSON. Once it's fixed from EP, will remove this method.
   * 
   * @param type $json_string
   * @return type
   */
  public function fixJson($json_string) {
    $json_string = preg_replace("/\s*:\s*{/", '":{', $json_string);
    $json_string = preg_replace("/'/", "\"", $json_string);
    $json_string = str_replace(': {', '":{', str_replace("'", '"', $json_string)) ;
    $json_string = str_replace('print', '"print"', str_replace('"print"', 'print', $json_string));
    $json_string = str_replace('size', '"size"', str_replace('"size"', 'size', $json_string));
    $json_string ="{\"" .  preg_replace('/\}\s*,\s*/', '}, "', $json_string). "}";;
    $json_string = str_replace("}}{", "},", $json_string);
    return $json_string;
  }

}
