<?php

namespace Drupal\cortex_api_layer\services;

use Drupal\Core\Config\ConfigManager;
use GuzzleHttp\Client;
use Drupal\user\PrivateTempStoreFactory;

/**
 * Class CortexApiAccessTokenService.
 */
class CortexApiAccessTokenService {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Drupal\user\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $userPrivateTempstore;

  /**
   * Constructs a new CortexApiAccessTokenService object.
   */
  public function __construct(ConfigManager $config_manager, Client $http_client, PrivateTempStoreFactory $user_private_tempstore) {
    $this->configManager = $config_manager;
    $this->httpClient = $http_client;
    $this->userPrivateTempstore = $user_private_tempstore;
    $this->configManager = \Drupal::config('cortex_api_layer.cortexapiconfig')->get();
  }

  /**
   *
   */
  public function generate() {
    // Private tempstorage to to capture session data.
    $private_tempstore = $this->userPrivateTempstore;
    // Getting Cortex API token.
    // Get URI.
    $config = \Drupal::config('cortex_api_layer.cortexapiconfig')->get();
    // Fetching Cortex API key from configurations.
    $url = $config['cortex_api_endpoint'] . $config['cortex_oauth2_uri'];
    // Giving a background call to get POST.
    try {
      $http_client = $this->httpClient->post($url, [
        'body' => 'username=divesh.kumar@testing.com&password=testing@123&grant_type=password&scope=' . $config['cortex_store_id'] . '&role=REGISTERED&',
        // 'body' => 'grant_type=password&scope=' . $config['cortex_store_id'] . '&role=PUBLIC',.
        'headers' => [
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
      ]
      );
    }
    catch (\Exception $e) {
      drupal_set_message($e->getMessage(), 'error', TRUE);
    }
    $response = $http_client->getBody()->getContents();
    // If response is not empty then set the authentication token once again.
    if (!empty($response)) {
      $response_decoded = \GuzzleHttp\json_decode($response, TRUE);
      $private_tempstore->get('cortex_api_layer')->set('authentication_token', $response_decoded['access_token']);
      drupal_set_message($response, 'status', TRUE);
    }
  }

  /**
   *
   */
  public function getAuthorizationHeader() {
    $private_tempstore = $this->userPrivateTempstore;
    // Getting Cortex API token.
    $cortex_api_token = $private_tempstore->get('cortex_api_layer')->get('authentication_token');
    // Returning API token.
    return "bearer " . $cortex_api_token;
  }

  /**
   *
   */
  public function getItemDefiniationURL() {
    return $_SERVER['HTTP_ORIGIN'] . "/";
  }

  /**
   * This method returns the items URI.
   */
  public function getItemsUrl() {
    $api_endpoint = $this->configManager['cortex_api_endpoint'];
    $store_id = $this->configManager['cortex_store_id'];
    $items_uri = $this->configManager['cortex_product_uri'];
    return $api_endpoint . $items_uri . '/' . $store_id;
  }

  /**
   *
   */
  public function getBasicCartURL() {
    $api_endpoint = $this->configManager['cortex_api_endpoint'];
    $store_id = $this->configManager['cortex_store_id'];
    $cart_uri = $this->configManager['cortex_cart_uri'];

    return $api_endpoint . $cart_uri . '/' . $store_id . '/default';
  }

  /**
   *
   */
  public function getCartId() {
    // Private tempstore.
    return 'default';
    $tempstore = \Drupal::service('user.private_tempstore')->get('cortex_api_layer');
    $cart_id = empty($tempstore->get('cart_id')) ? 'default' : $tempstore->get('cart_id');
    // Setup cart ID if not grabbed.
    if (empty($tempstore->get('cart_id'))) {
      drupal_set_message(t('Cart ID Fetched.'));
      // Cart URL.
      // Exception handling.
      try {
        $request = $this->httpClient->get($this->getBasicCartURL(), [
          'headers' => [
            'Authorization' => $this->getAuthorizationHeader(),
          ],
        ]);
      }
      catch (Exception $ex) {
        \Drupal::logger('Cortex:API:LoadingCart')->error($ex->getMessage());
        return $api_endpoint . $cart_uri . '/' . $store_id . '/default';
      }
      // If response is received then get the cart item and return it back.
      $response = json_decode($request->getBody()->getContents(), TRUE);
      // Update the cart url with Actual cart id.
      if (!empty($response)) {
        $cart_id = end(explode('/', $response['self']['uri']));
        \Drupal::service('user.private_tempstore')->get('cortex_api_layer')->set('cart_id', $cart_id);
      }

      $cart_id = $tempstore->get('cart_id');
      return $cart_id;
    }
    return $cart_id;
  }

  /**
   * This method returns cart URI.
   *
   * @return mixed
   */
  public function getCartUrl() {
    $tempstore = \Drupal::service('user.private_tempstore')->get('cortex_api_layer');
    $cart_id = $this->getCartId();
    $api_endpoint = $this->configManager['cortex_api_endpoint'];
    $store_id = $this->configManager['cortex_store_id'];
    $cart_uri = $this->configManager['cortex_cart_uri'];
    $cart_url = $api_endpoint . $cart_uri . '/' . $store_id . '/' . $cart_id;
    return $cart_url;
  }

  /**
   *
   */
  public function getItemPriceUrl($item_id) {
    $api_endpoint = $this->configManager['cortex_api_endpoint'];
    $store_id = $this->configManager['cortex_store_id'];
    $url = $api_endpoint . '/prices/items/' . $store_id . '/' . $item_id;
    return $url;
  }

  /**
   *
   */
  public function getItemDefinitionUrl($item_id) {
    $api_endpoint = $this->configManager['cortex_api_endpoint'];
    $store_id = $this->configManager['cortex_store_id'];
    $url = $api_endpoint . '/itemdefinitions/' . $store_id . '/' . $item_id;
    return $url;
  }

  /**
   *
   */
  public function getItemAvailabilityUrl($item_id) {
    $api_endpoint = $this->configManager['cortex_api_endpoint'];
    $store_id = $this->configManager['cortex_store_id'];
    $url = $api_endpoint . '/availabilities/items/' . $store_id . '/' . $item_id;
    return $url;
  }

  /**
   *
   */
  public function getItemOptionsUrl($item_id) {
    $api_endpoint = $this->configManager['cortex_api_endpoint'];
    $store_id = $this->configManager['cortex_store_id'];
    $url = $api_endpoint . '/itemdefinitions/' . $store_id . '/' . $item_id . '/options';
    return $url;
  }

  /**
   * This method returns the URL for items lookin batch.
   *
   * Input provided to the API is as "codes:[SKUS]"
   *
   * @return string
   */
  public function getItemLookupUrl() {
    $api_endpoint = $this->configManager['cortex_api_endpoint'];
    $store_id = $this->configManager['cortex_store_id'];
    $lookup_uri = $this->configManager['cortex_lookups_uri'];
    $url = $api_endpoint . $lookup_uri . '/' . $store_id . '/batches/items';
    $zoom = "?followlocation&zoom=element:definition,element:price,element:definition:options:element:value,element:definition:options:element";
    return $url . $zoom;
  }

}
