<?php

namespace Drupal\cortex_api_layer\services;

use Drupal\taxonomy\Entity\Term;
use Drupal\user\PrivateTempStoreFactory;
use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigManager;

/**
 * Class CortexApiCategoryImporterService.
 */
class CortexApiCategoryImporterService {

  /**
   * Drupal\user\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $userPrivateTempstore;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  protected $productService;

  /**
   * Constructs a new CortexApiCategoryImporterService object.
   */
  public function __construct(PrivateTempStoreFactory $user_private_tempstore, Client $http_client, ConfigManager $config_manager) {
    $this->userPrivateTempstore = $user_private_tempstore;
    $this->httpClient = $http_client;
    $this->configManager = $config_manager;
    $this->productService = \Drupal::service('cortex_api_layer.product_importer');
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function executeImport($vid) {

    $private_tempstore = $this->userPrivateTempstore;
    // Getting Cortex API token.
    $cortex_api_token = $private_tempstore->get('cortex_api_layer')->get('authentication_token');
    // If authentication token is empty then generate the API token.
    if (!empty($cortex_api_token)) {
      // Cortex API Authorization Data
      // Authorization Header.
      $authorization_header = "Bearer " . $cortex_api_token;
      // Get URI.
      $config = \Drupal::config('cortex_api_layer.cortexapiconfig')->get();
      // Fetching Cortex API key from configurations.
      $cortex_api_endpoint = $config['cortex_api_endpoint'];
      $store_id = $config['cortex_store_id'];
      $url = $cortex_api_endpoint . $config['cortex_navigation_uri'] . "/" . $store_id;
      $response = [];
      // Giving a background call to get POST.
      $http_client = $this->httpClient->get(
                  $url, [
                    'headers' => [
                      'Authorization' => $authorization_header,
                      'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                  ]
      );

      $response = $http_client->getBody()->getContents();

      // If response is not empty then set the authentication token once again.
      if (!empty($response)) {

        // Converting response to JSON.
        $response_decoded = \GuzzleHttp\json_decode($response, TRUE);
        // Getting Navigation Links.
        $links = $this->getNavigationLinks($response_decoded);

        foreach ($links as $link) {

          $http_client = \Drupal::httpClient()->get(
                      $link, [
                        'headers' => [
                          'Authorization' => $authorization_header,
                          'Content-Type' => 'application/x-www-form-urlencoded',
                        ],
                      ]
          );
          $parent = $http_client->getBody()->getContents();

          $parent = json_decode($parent, TRUE);
          // SEarching for the exsiting terms.
          $parent_term = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->loadByProperties([
            'vid' => $vid,
            'name' => $parent['display-name'],
          ]);
          // Capturing existing term if available otherwise create a new one.
          if (!empty($parent_term)) {
            // Creating Parent Term.
            $parent_term = array_shift($parent_term);
          }
          else {
            $parent_term = Term::create(
                                    [
                                      'vid' => $vid,
                                      'name' => $parent['display-name'],
                                    ]
            );
            $parent_term->save();
          }

          $child_links = $this->getChildNavigationLinks($parent);
          foreach ($child_links as $child_link) {
            //
            // Traversing the child terms.
            $http_client = \Drupal::httpClient()->get(
                        $child_link, [
                          'headers' => [
                            'Authorization' => $authorization_header,
                            'Content-Type' => 'application/x-www-form-urlencoded',
                          ],
                        ]
            );

            $child = $http_client->getBody()->getContents();
            $child = json_decode($child, TRUE);
            $child_term = \Drupal::service('cortex_api_layer.product_importer')->getTermByNameAndParent($child['display-name'], $parent_term->id());
            // If Child_term is empty then create it otherwise skip.
            if (empty($child_term)) {
              $child_term = Term::create(
                                      [
                                        'vid' => $vid,
                                        'name' => $child['display-name'],
                                        'parent' => $parent_term->id(),
                                      ]
              );

              $child_term->save();
            }
          }
        }
      }
      // You must to implement the logic of your REST Resource here.
    }
  }

  /**
   *
   */
  public function getNavigationLinks($response = []) {

    $output = [];
    foreach ($response['links'] as $link) {
      $output[] = $link['href'];
    }

    return $output;
  }

  /**
   *
   */
  public function getChildNavigationLinks($response = []) {

    $output = [];
    foreach ($response['links'] as $link) {
      if ($link['rel'] != 'child') {
        continue;
      }
      $output[] = $link['href'];
    }
    return $output;
  }

}
