<?php

namespace Drupal\cortex_api_layer\services;

// Use Drupal\Core\Entity\EntityTypeManager;.
use Drupal\node\Entity\Node;
use Drupal\Core\Config\ConfigManager;
use GuzzleHttp\Client;
use Drupal\user\PrivateTempStoreFactory;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class CortexApiProductImporter.
 */
class CortexApiProductImporter {

  const PRODUCT_SKU_SIZE_VID = 'size';

  /**
   * Drupal\cortex_api_layer\services\CortexApiLayerUrlService definition.
   *
   * @var Drupal\cortex_api_layer\services\CortexApiLayerUrlService
   */
  protected $cortexApiUrlService;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Drupal\user\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $userPrivateTempstore;
  protected $cortexApiService;
  protected $productService;
  protected $dbMapping;

  /**
   * Constructs a new CortexApiProductImporter object.
   */
  public function __construct(EntityTypeManager $entity_type_manager, ConfigManager $config_manager, Client $http_client, PrivateTempStoreFactory $user_private_tempstore, CortexApiAccessTokenService $cortex_api, CortexApiLayerUrlService $cortex_api_urls) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configManager = $config_manager;
    $this->httpClient = $http_client;
    $this->userPrivateTempstore = $user_private_tempstore;
    $this->cortexApiService = $cortex_api;
    $this->productService = \Drupal::service('cortex_api_layer.product');
    $this->cortexApiUrlService = $cortex_api_urls;
  }

  /**
   *
   */
  public static function getInstance() {
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $config_manager = \Drupal::service('config.manager');
    $http_client = \Drupal::service('http_client');
    $user_private_tempstore = \Drupal::service('user.private_tempstore');
    $cortex_api = \Drupal::service('cortex_api_layer.access_token');
    $cortex_api_urls = \Drupal::service('cortex_api_layer.urls');
    return new self($entity_type_manager, $config_manager, $http_client, $user_private_tempstore, $cortex_api, $cortex_api_urls);
  }

  /**
   *
   */
  public static function batchProcess($products_import_data) {//, &$context) {
    self::processProduct($products_import_data);
    $context['message'] = t('Importing products under :category', [':category' => $products_import_data['display-name']]);
    \Drupal::logger('ProductData')->info(print_r($products_import_data, 1));
  }

  /**
   *
   */
  public static function processProduct($product_data) {
    // Processing items for the basis catalog.
    if(!empty($product_data['_items'])) {
      self::processItems($product_data['_items']);
    }
    if(!empty($product_data['_child'])) {
      self::processCategoryItems($product_data['_child']);
    }
  }

  /**
   *
   */
  public static function processCategoryItems($categories) {
    foreach($categories as $category) {
      self::processItems($category['_items']);
    }
  }

  /**
   *
   */
  public static function processItems($items) {
    $self = self::getInstance();
    $product_service = $self->productService;
    // Getting Items for processing.
    $items = array_filter($items);

    foreach($items[0]['_element'] as $item) {
//     if ($item['_definition'][0]['display-name'] != 'Test Diapers') {
//       
//       continue;
//     }
      // Getting product definition to create a product.
      $product = $product_service->parseItemDefinition($item);
      $self->createProduct($product);
    }
  }

  /**
   *
   */
  public function executeBatchImport() {
    // Get all products information.
    $products_import_data = $this->getProductsInformation();

    $operations = [];

    // Composing batch operations.
    foreach($products_import_data as $product_data) {
      $operations[] = [
        ['Drupal\cortex_api_layer\services\CortexApiProductImporter', 'batchProcess'],
        [$product_data],
      ];
      //self::batchProcess($product_data, $operations);
    }
//    die('process');
    // Creating a batch object for processing.
    $batch = [
      'title' => t('Importing Products from ElasticPath Server'),
      'operations' => $operations,
      'finished' => drupal_set_message(t('Products have been imported successfully!')),
    ];
    // Setting up batch to execute.
    batch_set($batch);
    //batch_process('/admin/config/cortex_api_layer/product-importer-form');
  }

  /**
   * This method returns the list of products based on navigations API Response.
   *
   * @return mixed
   *
   * @throws \Exception
   */
  public function getProductsInformation() {
    // Initiate DB Mapping Locallay.
    // Storing private temporary storage for user data.
    $private_tempstore = $this->userPrivateTempstore;
    // Getting Cortex API token.
    $cortex_api_token = $private_tempstore->get('cortex_api_layer')->get('authentication_token');
    // If authentication token is empty then generate the API token.
    if(!empty($cortex_api_token)) {
      // Cortex API Authorization Data
      // Get URI.
      $config = $this->configManager->getConfigFactory()->get('cortex_api_layer.cortexapiconfig')->get();
      // Fetching Cortex API key from configurations.
      $response = [];
      $zoom = 'element,element:child:items:element:price,element:child:items:element:definition,element:child:items:element:definition:options:element,element:child:items:element:definition:prices,element:items:element:price,element:items:element:definition';
      try {
        // Giving a background call to get POST.
        $http_client = $this->httpClient->get($this->cortexApiUrlService->getNavigationsUrl() . "?zoom=" . $zoom, [
          'headers' => [
            'Authorization' => $this->cortexApiUrlService->getAuthorizationHeader(),
            'Content-Type' => 'application/x-www-form-urlencoded',
          ],
                    ]
        );
      }
      catch(\Exception $ex) {
        drupal_set_message($ex->getMessage(), 'error', TRUE);
        \Drupal::logger('Cortex:API:Navigations')->error($ex->getMessage());
        return;
      }
      // Getting response from service request.
      $response = json_decode($http_client->getBody()->getContents(), TRUE);

      if(!empty($response['_element'])) {
        // Return only valid data to import.
        return $this->filterValidDataforImport($response['_element']);
      }
      return [];
    }
    else {
      // Generate authentication token explicitly.
      \Drupal::service('cortex_api_layer.access_token')->generate();
      throw new \Exception(t('Cortex API: Invalid Authentication Token. Please re-generate authentication token.'));
    }
  }

  /**
   *
   */
  protected function filterValidDataforImport($raw_data) {
    $output = [];
    // Traverse through each data node and return if they have valid products to import
    // into drupal.
    foreach($raw_data as $category) {
      // If child has valid links or main category has items only then mark it as valid import data.
      if(!empty($category['_items']) || !empty($category['_child'])) {
        $output[] = $category;
      }
    }
    return $output;
  }

  /**
   * This method creates a single product.
   *
   * @param string $product_definition
   *   Product/Item data.
   *
   * @return mixed
   */
  protected function createProduct($product_definition) {
    // Product Skus.
    $product_skus = [];
    // Create SKUs First.
    if(!empty($product_definition['details']['skus'])) {
      $product_skus = $this->createItemSkus($product_definition['details']['skus']);
    }
    // Create Master Product.
    $this->createMasterProduct($product_definition, $product_skus);
  }

  /**
   *
   */
  public function createMasterProduct($product_definition, $product_skus) {
    // Category TErm to map this product against with.
    $category = $this->productService->parseItemCategories($product_definition['details']['tags']);
    // Product Node Array.
    $product_node_array = [
      'title' => $product_definition['display-name'],
      'type' => 'add_product',
      'field_product_price' => $product_definition['prices']['purchase-price']['amount'],
      'field_category' => $category,
      'field_itemcode' => $product_definition['item_code'],
      'field_product_code' => $product_definition['item_code'],
      'status' => 0,
    ];
    // Capturing the mapping record from database to decide for creation or updation of product.
    $product_mapping = $this->isProductAlreadyCreated($product_definition['item_code'], 'product');
    // If product mapping is empty then create a new product.
    if(empty($product_mapping)) {
      // Create new node initially with array.
      $product_node = Node::create($product_node_array);
      $product_node->save();
      // Adding an entry to the database.
      $this->addProductMappingToDB($product_definition['item_code'], $product_node->id(), 'product');
      // SKU mapping for master product.
      if(!empty($product_skus) && ($product_node instanceof Node)) {
        // Updating node for Product Reference fields for SKU mapping.
        foreach($product_skus as $sku) {
          $product_node->field_product_skus[] = ['target_id' => $sku];
        }
        // Saving Node Reference;.
        $product_node->save();
      }
    }
    else {
      $product_node = Node::load($product_mapping);
      $product_node->title = $product_definition['display-name'];
      $product_node->field_product_price->value = $product_definition['prices']['purchase-price']['amount'];
      $product_node->field_category->value = $category;
      $product_node->field_itemcode->value = $product_definition['item_code'];
      $product_node->field_product_code->value = $product_definition['item_code'];

      // If the reference is lost from the database then recreate this node.
      if(!$product_node instanceof Node) {
        // Composing product node.
        $product_node = $this->entityTypeManager->getStorage('node')->create($product_node_array);
        $product_node->save();
        // Adding Mapping to the DB.
        $this->addProductMappingToDB($product_definition['item_code'], $product_node->id(), 'product');

        if(!empty($product_skus) && ($product_node instanceof Node)) {
          // Updating node for Product Reference fields for SKU mapping.
          foreach($product_skus as $sku) {
            $product_node->field_product_skus[] = ['target_id' => $sku];
          }
          // Saving Node Reference;.
          $product_node->save();
        }
      }


      $product_node->save();
    }

//    return;
  }

  /**
   * This term returns term by name and parent reference.
   */
  public function getTermByNameAndParent($name, $parent = 0) {
    $connection = \Drupal::database();
    $query = "select fd.tid from taxonomy_term_field_data fd
                    inner join taxonomy_term_hierarchy th on th.tid = fd.tid 
                    where fd.name=:name and th.parent = :parent";
    $result = $connection->query($query, [':name' => $name, ':parent' => $parent])->fetch();

    if(!empty($result)) {
      return $result->tid;
    }

    return '';
  }

  /**
   * This method creates SKUS to a product.
   *
   * @param array $skus
   *   SKUS for a product.
   *
   * @return array
   *   List of SKU nodes to be added as references to master product.
   */
  public function createItemSkus($skus) {
    $sku_nodes = [];
    $zoom = "?followlocation&zoom=element:definition,element:price,element:definition:options:element:value,element:definition:options:element";

    // Lookup URL for SKUS.
    $lookup_url = $this->cortexApiUrlService->getItemsLookupUrl();

    // If SKUS are not empty then process.
    if(!empty($skus)) {
      // Exception handling.
      try {
        $request = $this->httpClient->post($lookup_url . $zoom, [
          'headers' => [
            'Authorization' => $this->cortexApiService->getAuthorizationHeader(),
            'Content-Type' => 'application/json',
          ],
          'body' => json_encode(['codes' => $skus]),
        ]);
      }
      catch(\Exception $ex) {
        \Drupal::logger('Cortex:API:ProductSKU')->error($ex->getMessage());
        drupal_set_message($ex->getMessage(), 'error', TRUE);
        return [];
      }

      // Checking the request status if that worked fine.
      if($request->getStatusCode() == 201) {
        $i = 0;
        // Fetching the response and converting it back to Array.
        $response = json_decode($request->getBody()->getContents(), TRUE);
        // If element is fetched out then parse all items thorough with that.
        if(!empty($response['_element'])) {
          // Process Multiple skus one after another.
          foreach($response['_element'] as $element) {
            // This will fetch the logical array of only required inforamtion from
            // response received from EP.
            $content = $this->productService->parseItemDefinition($element);
            $size_term_id = '';
            // Composing Node Structure Array for Product SKu.
            $product_sku_array = [
              'title' => !empty($content['display-name']) ? $content['display-name'] : $skus[$i],
              'type' => 'product_sku',
              'status' => 1,
              'field_price' => $content['prices']['purchase-price']['amount'],
              'field_sku' => $skus[$i],
              'field_itemcode' => $content['item_code'],
            ];
            // Loading Size for the product sku.
            if(!empty($content['options']['diaper_size'][0]['name'])) {
              $size_term_id = $this->getProductSizeId('size', $content['options']['diaper_size'][0]['name']);
              $product_sku_array['field_product_sizes'] = ['target_id' => $size_term_id];
            }
            // Updating Sku for Title & Sizes.
            $size_and_title = $this->updateSkuForTitleAndSizes($product_sku_array, $content['details']['sku_options']);
            // Fetching SKU group and title from SKU Options if they are available.
            if(!empty($size_and_title)) {
              $product_sku_array['title'] = $size_and_title['title'];
              $product_sku_array['field_sku_group'] = $size_and_title['field_sku_group'];
            }
            // Exception handling for Node saving.
            $existing_sku = $this->isProductAlreadyCreated($content['item_code'], 'sku');
            // If product mappig is found then get the nid and updat in the object.
            if(!empty($existing_sku)) {
              // Loading existing node for Update with latest data.
              $product_sku_node = Node::load($existing_sku);
              // There may be chances when a DB entry for Mapping exists however,
              // the mapped node gets removed then in that case it will create a new 
              // node.
              if(!$product_sku_node instanceof Node) {
                // Create a node for product sku.
                $product_sku_node = $this->entityTypeManager->getStorage('node')->create($product_sku_array);
                $product_sku_node->save();
                // Adding Mapping to the DB.
                $this->addProductMappingToDB($content['item_code'], $product_sku_node->id(), 'sku');
                // Increment the counter
                $i++;
                // Preparing a list of updated or newly created SKU nodes
                $sku_nodes[] = $product_sku_node->id();
                // It does not requires the code below needs to be executed
                // hence continue for the next iteration.
                continue;
                
              }
              // If a valid node is found then process the fields with latest data.
              $product_sku_node->title = !empty($content['display-name']) ? $content['display-name'] : $skus[$i];
              $product_sku_node->field_price->value = $content['prices']['purchase-price']['amount'];
              $product_sku_node->field_sku->value = $skus[$i];
              $product_sku_node->field_itemcode->value = $content['item_code'];
              // New title & size.
              $size_and_title = $this->updateSkuForTitleAndSizes($product_sku_node, $content['details']['sku_options'], TRUE);
              // IF SIZE AND TILE IS UPDATED.
              if(!empty($size_and_title)) {
                $product_sku_node->title = $size_and_title['title'];
                $product_sku_node->field_sku_group->value = $size_and_title['field_sku_group'];
              }
              // Loading Size for the product sku.
              if(!empty($content['options']['diaper_size'][0]['name'])) {
                $size_term_id = $this->getProductSizeId('size', $content['options']['diaper_size'][0]['name']);
                $product_sku_node->field_product_sizes = ['target_id' => $size_term_id];
              }
              // Save the udpated information to the node.
              $product_sku_node->save();
              \Drupal::logger('Cortex:ProductSKU:Updation')->info("ITEM: [" . $content['display-name'] . ']');
            }
            else {
              // This case executes when there is a completely new node is created
              // with fresh mapping to the db.
              $product_sku_node = $this->entityTypeManager->getStorage('node')->create($product_sku_array);
              $product_sku_node->save();
              $this->addProductMappingToDB($content['item_code'], $product_sku_node->id(), 'sku');
              \Drupal::logger('Cortex:ProductSKU:Creation')->info("ITEM: [" . $content['display-name'] . ']');
            }

            $i++;
            // Preparing a list of updated or newly created SKU nodes so that
            $sku_nodes[] = $product_sku_node->id();
          }
        }
      }
    }
    return $sku_nodes;
  }

  /**
   * This method adds a new record of product mapping to database table.
   *
   * @param mixed $itemcode
   * @param mixed $node_id
   */
  public function addProductMappingToDB($itemcode, $node_id, $type) {
    try {
      $database = \Drupal::database();
      $new_row = $database->insert('cpl_product_mapping')->fields(['ep_id', 'nid', 'type'])->values([
        'ep_id' => $itemcode,
        'nid' => $node_id,
        'type' => $type,
      ]);
      $new_row->execute();
    }
    catch(Exception $ex) {
      \Drupal::logger('Cortex:DB:ProductMapping')->error($ex->getMessage());
      print $ex->getMessage();
      exit;
    }
  }

  /**
   * This method checks the existing mapping of a product with EP in Drupal.
   *
   * @param mixed $itemcode
   *
   * @return mixed
   */
  protected function isProductAlreadyCreated($itemcode, $type) {
    // Database fetch from DB Mapping.
    $records = \Drupal::database()->query("SELECT ep_id, nid, type FROM {cpl_product_mapping} WHERE ep_id=:itemid AND type = :node_type", [':itemid' => $itemcode, ':node_type' => $type])->fetchAllAssoc('ep_id');
    // REturn if product is already mapped and return the node id.
    return !empty($records[$itemcode]) ? $records[$itemcode]->nid : '';
  }

  protected function updateSkuForTitleAndSizes($product_sku_array, $sku_info, $object = FALSE) {
    // Current SKU being imported.
    $sku = !$object ? $product_sku_array['field_sku'] : $product_sku_array->get('field_sku')->getString();
    // All sku optins information to match a string against specific details.
    $sku_info_keys = array_keys($sku_info);
    // Matching which sku is excatly matched with which sku option details.
    $matched_skus = array_map(function($key) use ($sku) {
      if(preg_match("/(" . $key . ")/", $sku, $output)) {
        return $output[0];
      }
    }, $sku_info_keys);

    // Eliminating blank values.
    if(!empty($matched_skus)) {
      // Fetchign the first record from the array.
      $matched_sku = array_shift(array_filter($matched_skus));
      $sizes = $sku_info[$matched_sku]['sizes'];
      // Getting Existing terms tids by query.
      $existing_terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties([
        'vid' => 'size',
        'name' => $sizes,
      ]);
      $existing_terms = !empty($existing_terms) ? array_keys($existing_terms) : [];
      // Database query to find out what term is not created if not then add it.
      $database = \Drupal::database();
      // FEtching all terms under sizes and give tids for the same.
      $terms = $database->query("SELECT name, tid FROM {taxonomy_term_field_data} td WHERE td.vid = :vid ", [':vid' => 'size'])->fetchAllAssoc('name', \PDO::FETCH_ASSOC);
      // Getting the term which are not part of sizes vocabuary.
      $non_existing_terms = array_diff($sizes, array_keys($terms));
      // Non existing terms creation.
      $new_terms = array_map(function($term) {
        $new_term = \Drupal\taxonomy\Entity\Term::create([
                      'vid' => 'size',
                      'name' => $term,
        ]);
        $new_term->save();
        return $new_term->id();
      }, $non_existing_terms);
      $final_terms = array_merge($existing_terms, $new_terms);
      // Updating the current object.
      if(!empty($sku_info[$matched_sku])) {
//        // Return as Array.
//        if(!$object) {
//          $product_sku_array['title'] = $sku_info[$matched_sku]['print'];
//          $product_sku_array['field_product_sizes'] = $final_terms;
//          $product_sku_array['field_sku_group'] = $matched_sku;
//        }
//        else {
        return [
          'title' => $sku_info[$matched_sku]['print'],
          'field_product_sizes' => $final_terms,
          'field_sku_group' => $matched_sku,
        ];
//        }
      }
    }
  }

  /**
   * This method search for a taxonomy term and if that is not not found then 
   * creates a new one and returns its TID otherwise returns the TID of existing
   * Term.
   * 
   * @param string $vid
   *  Vocabulary ID
   * 
   * @param string $term
   *  Taxonomy Term Name
   * 
   * @return numeric
   *  Term ID for the reference.
   */
  public function getProductSizeId($vid, $term) {
    try {
      $term_data = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties([
        'vid' => $vid,
        'name' => $term,
      ]);
      // If term is not found in the term store then create a new term under vocabulary provided
      // and return the tid.
      if(empty($term_data)) {
        // Creating a new taxonomy term.
        $new_term = \Drupal\taxonomy\Entity\Term::create([
                      'vid' => $vid,
                      'name' => $term,
        ]);
        // Save term to load the $new_term with latest data.
        $new_term->save();

        return $new_term->id();
      }
      else {
        // Return the first key found as there could be multiple keys as well.
        // TODO: need to enhace this logic.
        return array_shift(array_keys($term_data));
      }
    }
    catch(\Exception $e) {
      \Drupal::logger('Product Sizes')->error($e->getMessage());
      // Returns null if none of the cases are found.
      return '';
    }
  }


}
