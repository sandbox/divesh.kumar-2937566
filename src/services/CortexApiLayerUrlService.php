<?php

namespace Drupal\cortex_api_layer\services;

use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigManager;
use Drupal\user\PrivateTempStoreFactory;

/**
 * Class CortexApiLayerUrlService.
 */
class CortexApiLayerUrlService {

  const CORTEX_API_ENDPOINT = 'http://127.0.0.1:9080/cortex';
  const CORTEX_STORE_ID = 'mobee';
  const CORTEX_CART_URI = '/carts/!store/default';
  const CORTEX_PRODUCT_URI = '/items';
  const CORTEX_SEARCH_URI = '/search';
  const CORTEX_NAVIGATION_URI = '/navigations/!store';
  const CORTEX_AUTH_URI = '/oauth2/tokens';
  const CORTEX_LOOKUP_URI = '/lookups/!store/batches/items';
  const CORTEX_ACCOUNT_REGISTRATION = '/registrations/!store/newaccount/form?followlocation';
  const CORTEX_DEFAULT_ORDER = '/orders/!store/default?followlocation';
  const CORTEX_PURCHASE_ORDER = '/purchases/orders/!store';
  const CORTEX_PAYMENT_TOKEN_URL = '/paymenttokens/orders/!store';
  const CORTEX_ADD_TO_CART_URL = '/carts/!store/default/lineitems/items/!store';
  const CORTEX_ORDER_TAX_URL = '/taxes/orders/!store';

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * Drupal\user\privateTempStoreFactory definition.
   *
   * @var \Drupal\user\privateTempStoreFactory
   */
  protected $userPrivateTempStore;

  /**
   * Constructs a new CortexApiLayerUrlService object.
   */
  public function __construct(Client $http_client, ConfigManager $config_manager, PrivateTempStoreFactory $private_tempstore) {
    $this->httpClient = $http_client;
    $this->configManager = $config_manager;
    $this->userPrivateTempStore = $private_tempstore;
  }

  /**
   *
   */
  public function getUserProfileUrl() {
    return $this->_prepareUrl(self::CORTEX_ACCOUNT_REGISTRATION);
  }

  /**
   *
   */
  private function _prepareUrl($url) {
    // Loading configurations saved via Admin configuration form for Cortex API.
    $api_config_vars = $this->configManager->getConfigFactory()->get('cortex_api_layer.cortexapiconfig')->get();
    // Getting configured API endpoint.
    $api_endpoint = !empty($api_config_vars['cortex_api_endpoint']) ? $api_config_vars['cortex_api_endpoint'] : self::CORTEX_API_ENDPOINT;
    // Getting Configured Store ID.
    $store = !empty($api_config_vars['cortex_store_id']) ? $api_config_vars['cortex_store_id'] : self::CORTEX_STORE_ID;
    // Composed URL.
    return $api_endpoint . str_replace("!store", $store, $url);
  }

  /**
   * This returns an authorization header for API call to Cortex server.
   */
  public function getAuthorizationHeader() {
    // Getting Cortex API token.
    $cortex_api_token = $this->userPrivateTempStore->get('cortex_api_layer')->get('authentication_token');
    // Returning API token.
    return "bearer " . $cortex_api_token;
  }

  /**
   * This Method returns the URL for Default ORder.
   */
  public function getOrderUrl() {
    return $this->_prepareUrl(self::CORTEX_DEFAULT_ORDER);
  }

  /**
   * This Method returns the URL for Default ORder.
   */
  public function getCartUrl() {
    return $this->_prepareUrl(self::CORTEX_CART_URI);
  }

  /**
   * This method returns the URL for purchasing the order.
   */
  public function getPurchaseOrderUrl() {
    return $this->_prepareUrl(self::CORTEX_PURCHASE_ORDER);
  }

  /**
   * This method returns the URL for payment token creation.
   */
  public function getPaymentTokenUrl() {
    return $this->_prepareUrl(self::CORTEX_PAYMENT_TOKEN_URL);
  }

  /**
   * This method returns the URL for payment token creation.
   */
  public function getNavigationsUrl() {
    return $this->_prepareUrl(self::CORTEX_NAVIGATION_URI);
  }

  /**
   * This method returns the URL for Items lookup.
   */
  public function getItemsLookupUrl() {
    return $this->_prepareUrl(self::CORTEX_LOOKUP_URI);
  }

  /**
   * This method returns the URL for addToCart form.
   */
  public function getAddToCartUrl() {
    return $this->_prepareUrl(self::CORTEX_ADD_TO_CART_URL);
  }

  /**
   * This method returns the URL for addToCart form.
   */
  public function getOrderTaxUrl($order_id) {
    return $this->_prepareUrl(self::CORTEX_ORDER_TAX_URL) . '/' . $order_id;
  }
}
